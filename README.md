# Peanut Infection

This plugin is a port from the plugin [PeanutInfection](https://github.com/LewisTehMinerz/PeanutInfection) made by LewisTehMinerz.

## Installation

This plugin require [EXILED](https://github.com/galaxy119/EXILED).
Put the plugin into your Plugins folder.

## Config

|Key|Type|Default|Description|
|---|----|-------|-----------|
|`pi_enablebydefault`|Boolean|true|Enable the gamemode when the server start|
|`pi_enablerespawn`|Boolean|false|Enable the MTF or CI respawn|
|`pi_infectedhp`|Integer|800|How mush HP an infected have

## Command
|Command name|Arguments|Description|
|------------|---------|-----------|
|`peanutinfection`|on / enable / off / disable|Enable or disable the gamemode.|
